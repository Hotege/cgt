#include <cgt/matrix.h>

namespace cgt
{
    cgt_implement(matrix, object);

    matrix::matrix(size_t r, size_t c) : _rows(r), _cols(c)
    {
        _create();
    }

    matrix::matrix(const matrix& m) : matrix(m.rows(), m.cols())
    {
        for (size_t i = 0; i < m.rows(); i++)
            for (size_t j = 0; j < m.cols(); j++)
                at(i, j) = m.at(i, j);
    }

    matrix::~matrix()
    {
        _release();
    }

    matrix& matrix::operator=(const matrix& m)
    {
        _release();
        _rows = m.rows();
        _cols = m.cols();
        _create();
        for (size_t i = 0; i < m.rows(); i++)
            for (size_t j = 0; j < m.cols(); j++)
                at(i, j) = m.at(i, j);
        return *this;
    }

    bool matrix::operator==(const matrix& m) const
    {
        if (rows() != m.rows() || cols() != m.cols())
            return false;
        for (size_t i = 0; i < m.rows(); i++)
            for (size_t j = 0; j < m.cols(); j++)
                if (at(i, j) != m.at(i, j)) // TODO: maybe need precision
                    return false;
        return true;
    }

    matrix matrix::operator-() const
    {
        return reversed();
    }

    matrix matrix::operator+(const matrix& m) const
    {
        matrix ret = *this;
        if (rows() != m.rows() || cols() != m.cols())
            return ret;
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                ret.at(i, j) += m.at(i, j);
        return ret;
    }

    matrix matrix::operator-(const matrix& m) const
    {
        matrix ret = *this;
        return ret + (-m);
    }

    matrix matrix::operator*(double n) const
    {
        matrix ret = *this;
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                ret.at(i, j) *= n;
        return ret;
    }

    matrix matrix::operator/(double n) const
    {
        matrix ret = *this;
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                ret.at(i, j) /= n;
        return ret;
    }

    matrix& matrix::operator+=(const matrix& m)
    {
        if (rows() != m.rows() || cols() != m.cols())
            return *this;
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                at(i, j) += m.at(i, j);
        return *this;
    }

    matrix& matrix::operator-=(const matrix& m)
    {
        if (rows() != m.rows() || cols() != m.cols())
            return *this;
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                at(i, j) -= m.at(i, j);
        return *this;
    }

    matrix& matrix::operator*=(double n)
    {
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                at(i, j) *= n;
        return *this;
    }

    matrix& matrix::operator/=(double n)
    {
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                at(i, j) /= n;
        return *this;
    }

    vector matrix::operator*(const vector& v) const
    {
        vector ret(rows());
        if (cols() != v.dimension())
            return ret;
        for (size_t i = 0; i < rows(); i++)
        {
            ret[i] = 0;
            for (size_t j = 0; j < cols(); j++)
                ret[i] += at(i, j) * v[j];
        }
        return ret;
    }

    matrix matrix::operator*(const matrix& m) const
    {
        matrix ret(rows(), m.cols());
        if (cols() != m.rows())
            return ret;
        for (size_t i = 0; i < rows(); i++)
        {
            for (size_t j = 0; j < m.cols(); j++)
            {
                ret(i, j) = 0;
                for (size_t k = 0; k < cols(); k++)
                    ret(i, j) += at(i, k) * m.at(k, j);
            }
        }
        return ret;
    }

    double& matrix::operator()(size_t r, size_t c)
    {
        return at(r, c);
    }

    double matrix::operator()(size_t r, size_t c) const
    {
        return at(r, c);
    }

    double& matrix::at(size_t r, size_t c)
    {
        return _m[r][c];
    }

    double matrix::at(size_t r, size_t c) const
    {
        return _m[r][c];
    }

    size_t matrix::rows() const
    {
        return _rows;
    }

    size_t matrix::cols() const
    {
        return _cols;
    }

    void matrix::reverse()
    {
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                at(i, j) = -at(i, j);
    }

    matrix matrix::reversed() const
    {
        matrix m(*this);
        for (size_t i = 0; i < m.rows(); i++)
            for (size_t j = 0; j < m.cols(); j++)
                m.at(i, j) = -m.at(i, j);
        return m;
    }

    void matrix::transpose()
    {
        matrix m = transposed();
        if (m.rows() != rows() || m.cols() != cols())
        {
            _release();
            _rows = m.cols();
            _cols = m.rows();
            _create();
        }
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                at(i, j) = m.at(i, j);
    }

    matrix matrix::transposed() const
    {
        matrix ret(cols(), rows());
        for (size_t j = 0; j < cols(); j++)
            for (size_t i = 0; i < rows(); i++)
                ret.at(j, i) = at(i, j);
        return ret;
    }

    matrix matrix::cofactor(size_t r, size_t c) const
    {
        if (rows() == 1 || cols() == 1)
        {
            matrix ret(rows() == 1 ? 1 : rows() - 1,
                cols() == 1 ? 1 : cols() - 1);
            return ret;
        }
        matrix ret(rows() - 1, cols() - 1);
        for (size_t i = 0, ir = 0; i < rows(); i++)
        {
            if (i == r)
                continue;
            for (size_t j = 0, ic = 0; j < cols(); j++)
            {
                if (j == c)
                    continue;
                ret.at(ir, ic++) = at(i, j);
            }
            ir++;
        }
        return ret;
    }

    double matrix::det() const
    {
        if (rows() != cols())
            return 0;
        if (rows() == 1)
            return at(0, 0);
        else
        {
            double ret = 0;
            for (size_t j = 0; j < rows(); j++)
            {
                double Aij = at(0, j);
                matrix m = cofactor(0, j);
                ret += Aij * m.det() * double(j % 2 ? -1 : 1);
            }
            return ret;
        }
    }

    void matrix::inverse()
    {
        matrix m = inversed();
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                at(i, j) = m.at(i, j);
    }

    matrix matrix::inversed() const
    {
        matrix ret(*this);
        if (rows() != cols())
            return ret;
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
            {
                matrix c = cofactor(i, j);
                double A = c.det() * double(((i + j) % 2) ? -1 : 1);
                ret.at(i, j) = A;
            }
        return ret.transposed() / det();
    }

    void matrix::dump(FILE* fp) const
    {
        for (size_t i = 0; i < rows(); i++)
        {
            fprintf(fp, "|");
            for (size_t j = 0; j < cols(); j++)
            {
                fprintf(fp, "%g", at(i, j));
                if (j < cols() - 1)
                    fprintf(fp, " ");
            }
            fprintf(fp, "|\n");
        }
    }

    handle<object> matrix::copy() const
    {
        handle<object> ret = new matrix(*this);
        return ret;
    }

    void matrix::_create()
    {
        _m = new double* [rows()];
        for (size_t i = 0; i < rows(); i++)
            _m[i] = new double[cols()];
        for (size_t i = 0; i < rows(); i++)
            for (size_t j = 0; j < cols(); j++)
                at(i, j) = 0;
    }

    void matrix::_release()
    {
        for (size_t i = 0; i < rows(); i++)
        {
            delete[]_m[i];
            _m[i] = nullptr;
        }
        delete[]_m;
        _m = nullptr;
    }

    matrix operator*(double n, const matrix& m)
    {
        matrix ret = m;
        for (size_t i = 0; i < m.rows(); i++)
            for (size_t j = 0; j < m.cols(); j++)
                ret.at(i, j) *= n;
        return ret;
    }

    cgt_implement(matrix4x4, matrix);

    matrix4x4::matrix4x4() : matrix(4, 4)
    {
        for (size_t i = 0; i < rows(); i++)
            at(i, i) = 1;
    }

    matrix4x4::matrix4x4(const vector4d& r0, const vector4d& r1,
        const vector4d& r2, const vector4d& r3) : matrix4x4()
    {
        for (size_t i = 0; i < rows(); i++)
        {
            at(0, i) = r0[i];
            at(1, i) = r1[i];
            at(2, i) = r2[i];
            at(3, i) = r3[i];
        }
    }

    matrix4x4::matrix4x4(double a00, double a01, double a02, double a03,
        double a10, double a11, double a12, double a13,
        double a20, double a21, double a22, double a23,
        double a30, double a31, double a32, double a33) : matrix4x4()
    {
        at(0, 0) = a00; at(0, 1) = a01; at(0, 2) = a02; at(0, 3) = a03;
        at(1, 0) = a10; at(1, 1) = a11; at(1, 2) = a12; at(1, 3) = a13;
        at(2, 0) = a20; at(2, 1) = a21; at(2, 2) = a22; at(2, 3) = a23;
        at(3, 0) = a30; at(3, 1) = a31; at(3, 2) = a32; at(3, 3) = a33;
    }
}
