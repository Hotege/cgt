#include <cgt/point.h>

namespace cgt
{
    cgt_implement(point, geometry);
    
    point::point() : x(0), y(0), z(0)
    {
    }

    point::point(double _x, double _y, double _z) : point()
    {
        x = _x, y = _y, z = _z;
    }

    point::point(const vector3d& v)
    {
        x = v.x, y = v.y, z = v.z;
    }

    point& point::operator=(const point& pt)
    {
        x = pt.x, y = pt.y, z = pt.z;
        return *this;
    }

    bool point::operator==(const point& pt) const
    {
        if (x != pt.x || y != pt.y || z != pt.z) // TODO: maybe need precision
            return false;
        return true;
    }

    point point::operator-() const
    {
        return point(-x, -y, -z);
    }

    point point::operator+(const point& pt) const
    {
        point ret(x + pt.x, y + pt.y, z + pt.z);
        return ret;
    }

    point point::operator-(const point& pt) const
    {
        return *this + (-pt);
    }

    point point::operator*(double n) const
    {
        point ret(x * n, y * n, z * n);
        return ret;
    }

    point point::operator/(double n) const
    {
        point ret(x / n, y / n, z / n);
        return ret;
    }

    point& point::operator+=(const point& pt)
    {
        x += pt.x;
        y += pt.y;
        z += pt.z;
        return *this;
    }

    point& point::operator-=(const point& pt)
    {
        x -= pt.x;
        y -= pt.y;
        z -= pt.z;
        return *this;
    }

    point& point::operator*=(double n)
    {
        x *= n;
        y *= n;
        z *= n;
        return *this;
    }

    point& point::operator/=(double n)
    {
        x /= n;
        y /= n;
        z /= n;
        return *this;
    }

    void point::set(double _x, double _y, double _z)
    {
        x = _x, y = _y, z = _z;
    }

    void point::dump(FILE* fp) const
    {
        fprintf(fp, "(%g,%g,%g)", x, y, z);
    }

    handle<object> point::copy() const
    {
        handle<object> ret = new point(*this);
        return ret;
    }

    void point::transform(const matrix4x4& m)
    {
        vector4d v(x, y, z, 1);
        vector4d r = m * v;
        x = r[0], y = r[1], z = r[2];
    }
}
