#include <cgt/object.h>

namespace cgt
{
    const object::type object::_t_object = { typeid(object).hash_code(),
        nullptr };

    object::object()
    {
#if defined(_WIN32)
        (void)CoCreateGuid(&__id);
#else
        uuid_generate(&_id);
#endif
    }

    object::object(const object& obj) : object()
    {
    }

    object& object::operator=(const object& obj)
    {
        return *this;
    }

    bool object::operator==(const object& obj) const
    {
        return hash() == obj.hash();
    }

    bool object::is_kind_of(const type* _t) const
    {
        const type* t = get_type();
        while (t)
        {
            if (t == _t)
                return true;
            t = t->base;
        }
        return false;
    }

    uintptr_t object::hash() const
    {
        return uintptr_t(this);
    }

    uuid object::id() const
    {
        return __id;
    }

    void object::dump(FILE* fp) const
    {
#if defined(_WIN64)
        fprintf(fp, "<cgt::%s 0x%016llx>", get_name(), hash());
#else
        fprintf(fp, "<cgt::%s 0x%016x>", get_name(), hash());
#endif
    }
}
