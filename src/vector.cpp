#include <cgt/vector.h>
#include <cgt/point.h>

namespace cgt
{
    cgt_implement(vector, object);

    vector::vector(size_t d) : _dimension(d)
    {
        _create();
    }

    vector::vector(const vector& v) : vector(v._dimension)
    {
        for (size_t i = 0; i < v.dimension(); i++)
            at(i) = v.at(i);
    }

    vector::~vector()
    {
        _release();
    }

    vector& vector::operator=(const vector& v)
    {
        _release();
        _dimension = v.dimension();
        _create();
        for (size_t i = 0; i < v.dimension(); i++)
            at(i) = v.at(i);
        return *this;
    }

    bool vector::operator==(const vector& v) const
    {
        if (dimension() != v.dimension())
            return false;
        for (size_t i = 0; i < v.dimension(); i++)
            if (at(i) != v.at(i)) // TODO: maybe need precision
                return false;
        return true;
    }

    vector vector::operator-() const
    {
        return reversed();
    }

    vector vector::operator+(const vector& v) const
    {
        vector ret(*this);
        if (dimension() != v.dimension())
            return ret;
        for (size_t i = 0; i < dimension(); i++)
            ret.at(i) += v.at(i);
        return ret;
    }

    vector vector::operator-(const vector& v) const
    {
        return *this + (-v);
    }

    vector vector::operator*(double n) const
    {
        vector ret(*this);
        for (size_t i = 0; i < dimension(); i++)
            ret.at(i) *= n;
        return ret;
    }

    vector vector::operator/(double n) const
    {
        vector ret(*this);
        for (size_t i = 0; i < dimension(); i++)
            ret.at(i) /= n;
        return ret;
    }

    vector& vector::operator+=(const vector& v)
    {
        if (dimension() != v.dimension())
            return *this;
        for (size_t i = 0; i < dimension(); i++)
            at(i) += v.at(i);
        return *this;
    }

    vector& vector::operator-=(const vector& v)
    {
        if (dimension() != v.dimension())
            return *this;
        for (size_t i = 0; i < dimension(); i++)
            at(i) -= v.at(i);
        return *this;
    }

    vector& vector::operator*=(double n)
    {
        for (size_t i = 0; i < dimension(); i++)
            at(i) *= n;
        return *this;
    }

    vector& vector::operator/=(double n)
    {
        for (size_t i = 0; i < dimension(); i++)
            at(i) /= n;
        return *this;
    }

    double vector::operator*(const vector& v) const
    {
        return dot(v);
    }

    double& vector::operator[](size_t idx)
    {
        return at(idx);
    }

    double vector::operator[](size_t idx) const
    {
        return at(idx);
    }

    double& vector::at(size_t idx)
    {
        return _v[idx];
    }

    double vector::at(size_t idx) const
    {
        return _v[idx];
    }

    size_t vector::dimension() const
    {
        return _dimension;
    }

    void vector::reverse()
    {
        for (size_t i = 0; i < dimension(); i++)
            at(i) = -at(i);
    }

    vector vector::reversed() const
    {
        vector ret(dimension());
        for (size_t i = 0; i < dimension(); i++)
            ret.at(i) = -at(i);
        return ret;
    }

    double vector::dot(const vector& v) const
    {
        double ret = 0;
        if (dimension() != v.dimension())
            return ret;
        for (size_t i = 0; i < dimension(); i++)
            ret += at(i) * v.at(i);
        return ret;
    }

    double vector::square_magnitude() const
    {
        double ret = 0;
        for (size_t i = 0; i < dimension(); i++)
            ret += at(i) * at(i);
        return ret;
    }

    double vector::magnitude() const
    {
        return sqrt(square_magnitude());
    }

    void vector::normalize()
    {
        double m = magnitude();
        for (size_t i = 0; i < dimension(); i++)
            at(i) /= m;
    }

    vector vector::normalized() const
    {
        vector ret(*this);
        ret.normalize();
        return ret;
    }

    void vector::dump(FILE* fp) const
    {
        fprintf(fp, "(");
        for (size_t i = 0; i < dimension(); i++)
            fprintf(fp, i < dimension() - 1 ? "%g," : "%g", at(i));
        fprintf(fp, ")");
    }

    handle<object> vector::copy() const
    {
        handle<object> ret = new vector(*this);
        return ret;
    }

    void vector::_create()
    {
        _v = new double[dimension()];
        for (size_t i = 0; i < dimension(); i++)
            at(i) = 0;
    }

    void vector::_release()
    {
        delete[]_v;
        _v = nullptr;
    }

    vector operator*(double n, const vector& v)
    {
        vector ret(v);
        for (size_t i = 0; i < v.dimension(); i++)
            ret.at(i) *= n;
        return ret;
    }

    cgt_implement(vector2d, vector);
    const vector2d vector2d::zero = vector2d(0, 0);
    const vector2d vector2d::x_axis = vector2d(1, 0);
    const vector2d vector2d::y_axis = vector2d(0, 1);

    vector2d::vector2d() : vector(2), x(0), y(0)
    {
    }

    vector2d::vector2d(const vector& v) : vector2d()
    {
        for (int i = 0; i < v.dimension() && i < dimension(); i++)
            at(i) = v.at(i);
    }

    vector2d::vector2d(double _x, double _y) : vector2d()
    {
        x = _x, y = _y;
    }

    double& vector2d::at(size_t idx)
    {
        if (idx == 0)
            return x;
        else if (idx == 1)
            return y;
        else
            return x;
    }

    double vector2d::at(size_t idx) const
    {
        if (idx == 0)
            return x;
        else if (idx == 1)
            return y;
        else
            return x;
    }

    void vector2d::set(double _x, double _y)
    {
        x = _x, y = _y;
    }

    cgt_implement(vector3d, vector);
    const vector3d vector3d::zero = vector3d(0, 0, 0);
    const vector3d vector3d::x_axis = vector3d(1, 0, 0);
    const vector3d vector3d::y_axis = vector3d(0, 1, 0);
    const vector3d vector3d::z_axis = vector3d(0, 0, 1);

    vector3d::vector3d() : vector(3), x(0), y(0), z(0)
    {
    }

    vector3d::vector3d(const vector& v) : vector3d()
    {
        for (int i = 0; i < v.dimension() && i < dimension(); i++)
            at(i) = v.at(i);
    }

    vector3d::vector3d(const point& pt) : vector3d()
    {
        x = pt.x, y = pt.y, z = pt.z;
    }

    vector3d::vector3d(const point& s, const point& e) : vector3d()
    {
        point dir = e - s;
        x = dir.x, y = dir.y, z = dir.z;
    }

    vector3d::vector3d(double _x, double _y, double _z) : vector3d()
    {
        x = _x, y = _y, z = _z;
    }

    vector3d vector3d::operator^(const vector3d& v) const
    {
        return cross(v);
    }

    double& vector3d::at(size_t idx)
    {
        if (idx == 0)
            return x;
        else if (idx == 1)
            return y;
        else if (idx == 2)
            return z;
        else
            return x;
    }

    double vector3d::at(size_t idx) const
    {
        if (idx == 0)
            return x;
        else if (idx == 1)
            return y;
        else if (idx == 2)
            return z;
        else
            return x;
    }

    void vector3d::set(double _x, double _y, double _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    vector3d vector3d::cross(const vector3d& v) const
    {
        return vector3d(y * v.z - z * v.y,
            z * v.x - x * v.z,
            x * v.y - y * v.x);
    }

    cgt_implement(vector4d, vector);
    const vector4d vector4d::zero = vector4d(0, 0, 0, 0);
    const vector4d vector4d::x_axis = vector4d(1, 0, 0, 0);
    const vector4d vector4d::y_axis = vector4d(0, 1, 0, 0);
    const vector4d vector4d::z_axis = vector4d(0, 0, 1, 0);
    const vector4d vector4d::w_axis = vector4d(0, 0, 0, 1);

    vector4d::vector4d() : vector(4), x(0), y(0), z(0), w(0)
    {
    }

    vector4d::vector4d(const vector& v) : vector4d()
    {
        for (int i = 0; i < v.dimension() && i < dimension(); i++)
            at(i) = v.at(i);
    }

    vector4d::vector4d(double _x, double _y, double _z, double _w) : vector4d()
    {
        x = _x, y = _y, z = _z, w = _w;
    }

    vector4d::vector4d(const vector3d& v, double _w) : vector4d()
    {
        x = v.x, y = v.y, z = v.z, w = _w;
    }

    double& vector4d::at(size_t idx)
    {
        if (idx == 0)
            return x;
        else if (idx == 1)
            return y;
        else if (idx == 2)
            return z;
        else if (idx == 3)
            return w;
        else
            return x;
    }

    double vector4d::at(size_t idx) const
    {
        if (idx == 0)
            return x;
        else if (idx == 1)
            return y;
        else if (idx == 2)
            return z;
        else if (idx == 3)
            return w;
        else
            return x;
    }

    void vector4d::set(double _x, double _y, double _z, double _w)
    {
        x = _x;
        y = _y;
        z = _z;
        w = _w;
    }
}
