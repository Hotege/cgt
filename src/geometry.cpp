#include <cgt/geometry.h>

namespace cgt
{
    cgt_implement(geometry, object);

    int geometry::dimension() const
    {
        return 3;
    }
}
