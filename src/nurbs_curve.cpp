#include <cgt/nurbs_curve.h>

namespace cgt
{
    cgt_implement(nurbs_curve, geometry);

    nurbs_curve::nurbs_curve(size_t degree,
        const point* poles, size_t poles_count,
        const double* knots, size_t knots_count,
        const double* weights)
    {
        _create(degree, poles, poles_count, knots, knots_count, weights);
    }

    nurbs_curve::nurbs_curve(const nurbs_curve& c)
    {
        _create(c._degree,
            c._poles, c._poles_count, c._knots, c._knots_count, c._weights);
    }

    nurbs_curve::~nurbs_curve()
    {
        _release();
    }

    handle<object> nurbs_curve::copy() const
    {
        handle<object> ret = new nurbs_curve(*this);
        return ret;
    }

    void nurbs_curve::transform(const matrix4x4& m)
    {
        if (!_poles)
            return;
        for (size_t i = 0; i < _poles_count; i++)
            _poles[i].transform(m);
    }

    void nurbs_curve::_create(size_t degree,
        const point* poles, size_t poles_count,
        const double* knots, size_t knots_count,
        const double* weights)
    {
        _degree = degree;
        _poles = new point[poles_count];
        _poles_count = poles_count;
        _weights = new double[poles_count];
        for (size_t i = 0; i < poles_count; i++)
        {
            _poles[i] = poles[i];
            if (weights)
                _weights[i] = weights[i];
            else
                _weights[i] = 1;
        }
        _knots = new double[knots_count];
        _knots_count = knots_count;
        for (size_t i = 0; i < knots_count; i++)
            _knots[i] = knots[i];
    }

    void nurbs_curve::_release()
    {
        delete[]_poles;
        _poles = nullptr;
        delete[]_weights;
        _weights = nullptr;
        delete[]_knots;
        _knots = nullptr;
    }
}
