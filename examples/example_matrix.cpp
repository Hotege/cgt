
#include <cgt/cgt.h>

#if defined(_WIN32) || defined(__MINGW32__) || defined(__MINGW64__)
void dump_id(cgt::uuid guid)
{
    printf("%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x\n",
        guid.Data1, guid.Data2, guid.Data3,
        guid.Data4[0], guid.Data4[1],
        guid.Data4[2], guid.Data4[3],
        guid.Data4[4], guid.Data4[5],
        guid.Data4[6], guid.Data4[7]);
}
#else
void dump_id(cgt::uuid guid)
{
}
#endif

int main()
{
    cgt::matrix4x4 m1;
    cgt::matrix4x4 m2;
    m2(1, 1) = 2;
    cgt::matrix4x4 m3;
    m3 = m1;
    m3(2, 2) = 3;
    cgt::matrix4x4 m4(m2);
    m4(0, 1) = 4;
    m4(0, 3) = 2;
    m4(1, 3) = 3;
    m4(2, 3) = 1;
    dump_id(m1.id());
    m1.dump(stdout);
    dump_id(m2.id());
    m2.dump(stdout);
    dump_id(m3.id());
    m3.dump(stdout);
    dump_id(m4.id());
    m4.dump(stdout);
    cgt::matrix4x4 identity;
    printf("m1 == identity: %d\n", m1 == identity);
    cgt::vector4d v4(4, 3, 2, 1);
    (m4 * v4).dump(stdout);
    printf("\n");
    printf("inverse of m4:\n");
    m4.inversed().dump(stdout);
    printf("m2 x m4:\n");
    (m2 * m4).dump(stdout);
    return 0;
}
