
#include <cgt/cgt.h>

int main()
{
    cgt::vector2d v2(3, 4);
    printf("v2: ");
    v2.dump(stdout);
    printf(" and its length is %g\n", v2.magnitude());
    cgt::vector2d v22(8, 15);
    printf("v22: ");
    v22.dump(stdout);
    printf(" and its length is %g\n", v22.magnitude());
    printf("v2 dot v22 = %g\n", v2 * v22);

    cgt::vector3d v3(3, 4, 12);
    printf("v3: ");
    v3.dump(stdout);
    printf(" and its length is %g\n", v3.magnitude());
    cgt::vector3d v33(2, 3, 4);
    printf("v33: ");
    v33.dump(stdout);
    printf(" and its length is %g\n", v33.magnitude());
    printf("cross of v3 and v33: ");
    (v3 ^ v33).dump(stdout);
    printf("\n");

    cgt::vector4d v4 = v3;
    v4.w = 84;
    printf("v4: ");
    v4.dump(stdout);
    printf(" and its length is %g\n", v4.magnitude());
    return 0;
}
