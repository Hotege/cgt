/**
 * @file object.h
 * @copyright (c) Hotege
 * 
 * CGT object class
 * CGT object is the most basic class in CGT.
 * It derives various classes to implement the construction and calculation of
 * geometry and topology.
*/

#if !defined(_CGT_OBJECT_H_)
#define _CGT_OBJECT_H_

#include "cgtdef.h"
#include "handle.h"

#define cgt_claim(name) \
public: \
static const object::type _t_##name; \
static const object::type* desc() { return &name::_t_##name; } \
virtual const object::type* get_type() const { return &name::_t_##name; } \
virtual const char* get_name() const { return #name; }

namespace cgt
{
   /**
    * @class object
    * @brief the most basic class in CGT
   */
    class CGT_API object
    {
    public:
       /**
        * @struct type
        * @brief the type class of object
       */
        typedef struct _type
        {
            /**
             * @brief the unique identifier of object type
            */
            size_t hash;
            /**
             * @brief the pointer that points to the parent class
            */
            const _type* base = nullptr;
        } type;
        cgt_claim(object);
    public:
        /**
         * @brief constructor of object
        */
        object();
        /**
         * @brief copy constructor of object
        */
        object(const object& obj);

        /**
         * @brief assignment operator
         * @param obj the object assigned to the target
         * @note the assignment should not copy the __id
        */
        object& operator=(const object& obj);
        /**
         * @brief check whether two objects are equal
         * @param obj the object compared
         * @return boolean
         * @note will check whether the pointers are equal by default
        */
        bool operator==(const object& obj) const;

        /**
         * @brief check whether a object is a class or derived class
         * @param _t the exact type
         * @return boolean
        */
        bool is_kind_of(const type* _t) const;
        /**
         * @brief get the hash code of a object
         * @return the hash code
         * @note will return the integerized pointer
        */
        uintptr_t hash() const;
        /**
         * @brief get the id of a object
         * @return uuid
        */
        uuid id() const;
        /**
         * @brief dump the object to a stream
         * @param fp the handle of a stream
        */
        virtual void dump(FILE* fp) const;
        /**
         * @brief copy and create a new object from current object
         * @return handler of the new object
        */
        virtual handle<object> copy() const = 0;
    private:
        /**
         * @brief the address operator
         * @return the pointer of a object
         * @note we delete it and use cgt::handle instead
        */
        object* operator&() = delete;

        /**
         * @brief the id of a object
        */
        uuid __id;
    };
}

#define cgt_implement(name, base) \
const object::type name::_t_##name = { typeid(name).hash_code(),\
    &base::_t_##base }

#endif
