/**
 * @file cgt.h
 * @copyright (c) Hotege
 * 
 * CGT Including files header.
 * This file just includes the header files in CGT Library.
*/

#if !defined(_CGT_H_)
#define _CGT_H_

#include "handle.h"
#include "object.h"
#include "geometry.h"
#include "matrix.h"
#include "vector.h"

#endif
