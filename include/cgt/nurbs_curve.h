/**
 * @file nurbs_curve.h
 * @copyright (c) Hotege
 * 
 * CGT NURBS curve class
 * NURBS curve means Non-Uniform Rational B-Spline curve
 * NURBS curve can represent all kinds of curves with control points' weights
*/

#if !defined(_CGT_NURBS_CURVE_H_)
#define _CGT_NURBS_CURVE_H_

#include "object.h"
#include "geometry.h"
#include "point.h"

namespace cgt
{
    /**
     * @class nurbs_curve
     * @brief NURBS curve class, derived from class geometry.
     * 
     * NURBS curve is defined by degree, control points,
     * knots vector (with multiplicities) and weights.
    */
    class CGT_API nurbs_curve : public geometry
    {
        cgt_claim(nurbs_curve);
    public:
        /**
         * @brief the creation function
         * @param degree the degree of the NURBS curve
         * @param poles the control points
         * @param poles_count the size of the control points
         * @param knots the knots vector
         * @param knots_count the size of the knots vector
         * @param weights the weights
         * @note create new memory instead of referring to the pointer
         * @note all weights of the control points are equal if weights is null
        */
        nurbs_curve(size_t degree,
            const point* poles, size_t poles_count,
            const double* knots, size_t knots_count,
            const double* weights = nullptr);
        /**
         * @brief copy constructor
         * @param c the source curve
        */
        nurbs_curve(const nurbs_curve& c);
        /**
         * @brief destructor
        */
        virtual ~nurbs_curve();

        /**
         * @brief copy and create a new curve from current curve (as object)
         * @return handler of the new curve
        */
        virtual handle<object> copy() const override;
        /**
         * @brief transformation of curve
         * @param m the transformation matrix
        */
        virtual void transform(const matrix4x4& m) override;
    protected:
        /**
         * @brief the creation function
         * @param degree the degree of the NURBS curve
         * @param poles the control points
         * @param poles_count the size of the control points
         * @param knots the knots vector
         * @param knots_count the size of the knots vector
         * @param weights the weights
         * @note create new memory instead of referring to the pointer
         * @note all weights of the control points are equal if weights is null
        */
        void _create(size_t degree,
            const point* poles, size_t poles_count,
            const double* knots, size_t knots_count,
            const double* weights = nullptr);
        /**
         * @brief the release function
         * @note free the allocated memory
        */
        void _release();
    protected:
        /**
         * @brief the degree of the NURBS curve
        */
        size_t _degree = 0;
        /**
         * @brief the control points
        */
        point* _poles = nullptr;
        /**
         * @brief the size of the control points
        */
        size_t _poles_count = 0;
        /**
         * @brief the weights
        */
        double* _weights = nullptr;
        /**
         * @brief the knots vector
        */
        double* _knots = nullptr;
        /**
         * @brief the size of the knots vector
        */
        size_t _knots_count = 0;
    };
}

#endif
