/**
 * @file handle.h
 * @copyright (c) Hotege
 * 
 * CGT pointer handler template class
 * CGT object has disabled address operator.
 * Use handler template class instead.
*/

#if !defined(_CGT_HANDLE_H_)
#define _CGT_HANDLE_H_

#include "cgtdef.h"
#include <mutex>

namespace cgt
{
    /**
     * @class handle
     * @brief a multi-threads-secure template class
    */
    template <typename _T>
    class handle
    {
        /**
         * @brief the type of itself
        */
        using _self = handle<_T>;
    public:
        /**
         * @brief constructor of handler
         * @param ptr the source pointer
        */
        handle(_T* ptr = nullptr) : __ptr(ptr), __count(new int(1)),
            __mutex(new std::mutex)
        {
        }
        /**
         * @brief copy constructor of handler
         * @param s the source handler
        */
        handle(const _self& s) : __ptr(s.__ptr), __count(s.__count),
            __mutex(s.__mutex)
        {
            (*__mutex).lock();
            ++(*__count);
            (*__mutex).unlock();
        }
        /**
         * @brief destructor of vector
        */
        ~handle()
        {
            release();
        }
        /**
         * @brief function to release resources of handler
        */
        void release()
        {
            (*__mutex).lock();
            bool flag = false;
            if (--(*__count) == 0 && __ptr && __count)
            {
                delete __ptr;
                __ptr = nullptr;
                delete __count;
                __count = nullptr;
                flag = true;
            }
            (*__mutex).unlock();
            if (flag)
            {
                delete __mutex;
                __mutex = nullptr;
            }
        }
        /**
         * @brief assignment operator
         * @param s the source handler
         * @return return the handler itself
        */
        _self& operator=(const _self& s)
        {
            if (__ptr != s.__ptr)
            {
                release();
                __ptr = s.__ptr;
                __count = s.__count;
                __mutex = s.__mutex;
                (*__mutex).lock();
                ++(*__count);
                (*__mutex).unlock();
            }
            return *this;
        }
        /**
         * @brief reference (to instance pointer) operator
         * @return the instance pointer
        */
        _T* operator->()
        {
            return __ptr;
        }
        /**
         * @brief reference (to instance object) operator
         * @return the instance object (referenced)
        */
        _T& operator*()
        {
            return *__ptr;
        }
        /**
         * @brief get the constant instance pointer
         * @return the constant instance pointer
        */
        _T* get() const
        {
            return __ptr;
        }
    private:
        /**
         * @brief the pointer of an exact object
        */
        _T* __ptr = nullptr;
        /**
         * @brief the count of the times of reference
        */
        int* __count = nullptr;
        /**
         * @brief the mutex object, ensuring multi threads secure
        */
        std::mutex* __mutex = nullptr;
    };
}

#endif
