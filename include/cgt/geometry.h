/**
 * @file geometry.h
 * @copyright (c) Hotege
 * 
 * CGT geometry class
 * geometry is the basic class of all geometry shapes
*/

#if !defined(_CGT_GEOMETRY_H_)
#define _CGT_GEOMETRY_H_

#include "object.h"
#include "matrix.h"

namespace cgt
{
    /**
     * @class geometry
     * @note the basic class of geometry, derived from class object
    */
    class CGT_API geometry : public object
    {
        cgt_claim(geometry);
    public:
        /**
         * @brief transformation of geometry shapes
         * @param m the transformation matrix
         * @note implement varies with different geometry shapes
        */
        virtual void transform(const matrix4x4& m) = 0;
        /**
         * @brief get the dimension of a geometry shape
         * @return the dimension
        */
        virtual int dimension() const;
    };
}

#endif
