/**
 * @file matrix.h
 * @copyright (c) Hotege
 * 
 * CGT matrix classes
 * matrix is a term using rectangular array or table to represent a mathematical
 * object.
*/

#if !defined(_CGT_MATRIX_H_)
#define _CGT_MATRIX_H_

#include "object.h"
#include "vector.h"

namespace cgt
{
    /**
     * @class matrix
     * @brief the basic class of matrix, derived from class object
    */
    class CGT_API matrix : public object
    {
        cgt_claim(matrix);
    public:
        /**
         * @brief constructor of matrix
         * @param r the rows of matrix
         * @param c the cols of matrix
         * @note the rows and cols should be larger than 0
        */
        explicit matrix(size_t r, size_t c);
        /**
         * @brief copy constructor of matrix
         * @param m the source matrix
        */
        matrix(const matrix& m);
        /**
         * @brief destructor of matrix
        */
        virtual ~matrix();

        /**
         * @brief assignment operator
         * @param m the source matrix
         * @return matrix itself
         * @note this method will re-allocate the inside memory of matrix
        */
        matrix& operator=(const matrix& m);
        /**
         * @brief comparison of two matrices
         * @param m the matrix compared
         * @return boolean
         * @note this method will check each element in two matrices
        */
        bool operator==(const matrix& m) const;
        /**
         * @brief opposition operator
         * @return the opposited matrix
         * @note will return matrix that components are reversed
         * @sa matrix::reversed()
        */
        matrix operator-() const;
        /**
         * @brief addition operator
         * @param v the addend matrix
         * @return the result matrix
         * @note the rows and cols of two matrices should be equal
         * @note return itself if the rows or cols are not equal
        */
        matrix operator+(const matrix& m) const;
        /**
         * @brief subtraction operator
         * @param v the subtrahend matrix
         * @return the result matrix
         * @note the rows and cols of two matrices should be equal
         * @note return minuend if the rows or cols are not equal
        */
        matrix operator-(const matrix& m) const;
        /**
         * @brief multiplication operator
         * @param n the multiplier number
         * @return the result matrix
        */
        matrix operator*(double n) const;
        /**
         * @brief division operator
         * @param n the divisor number
         * @return the result matrix
        */
        matrix operator/(double n) const;
        /**
         * @brief self addition operator
         * @param v the addend matrix
         * @return the result matrix (itself)
         * @note the rows and cols of two matrices should be equal
         * @note return itself if the rows or cols are not equal
        */
        matrix& operator+=(const matrix& m);
        /**
         * @brief self subtraction operator
         * @param v the subtrahend matrix
         * @return the result matrix (itself)
         * @note the rows and cols of two matrices should be equal
         * @note return minuend if the rows or cols are not equal
        */
        matrix& operator-=(const matrix& m);
        /**
         * @brief self multiplication operator
         * @param n the multiplier number
         * @return the result matrix (itself)
        */
        matrix& operator*=(double n);
        /**
         * @brief self division operator
         * @param n the divisor number
         * @return the result matrix (itself)
        */
        matrix& operator/=(double n);
        /**
         * @brief multiplication operator (matrix dot vector)
         * @param v the vector
         * @return the result vector
        */
        vector operator*(const vector& v) const;
        /**
         * @brief multiplication operator (matrix1 dot matrix2)
         * @param m the matrix
         * @return the result matrix
        */
        matrix operator*(const matrix& m) const;
        /**
         * @brief index operator
         * @param r the row
         * @param c the col
         * @return the component
         * @sa matrix::at(size_t r, size_t c)
        */
        double& operator()(size_t r, size_t c);
        /**
         * @brief index operator
         * @param r the row
         * @param c the col
         * @return the constant component
         * @sa matrix::at(size_t r, size_t c)
        */
        double operator()(size_t r, size_t c) const;

        /**
         * @brief index function
         * @param r the row
         * @param c the col
         * @return the component
        */
        double& at(size_t r, size_t c);
        /**
         * @brief index function
         * @param r the row
         * @param c the col
         * @return the constant component
        */
        double at(size_t r, size_t c) const;
        /**
         * @brief get the rows of a matrix
         * @return the rows
        */
        size_t rows() const;
        /**
         * @brief get the cols of a matrix
         * @return the cols
        */
        size_t cols() const;
        /**
         * @brief opposition function
         * @return the opposited matrix
         * @note will reverse all signs of elements in the vector
        */
        void reverse();
        /**
         * @brief opposition function
         * @return the opposited matrix
         * @note will return matrix that elements are reversed
        */
        matrix reversed() const;
        /**
         * @brief swap rows data and cols data
        */
        void transpose();
        /**
         * @brief swap rows data and cols data
         * @return the transposed matrix
        */
        matrix transposed() const;
        /**
         * @brief get the cofactor matrix
         * @param r the row
         * @param c the col
         * @return the cofactor matrix
         * @note the new rows of cofactor matrix is current rows - 1
         * @note the new cols of cofactor matrix is current cols - 1
        */
        matrix cofactor(size_t r, size_t c) const;
        /**
         * @brief calculate the determinant of a matrix
         * @return the determinant
        */
        double det() const;
        /**
         * @brief inverse the matrix
        */
        void inverse();
        /**
         * @brief get the inversed matrix
         * @return the inversed matrix
        */
        matrix inversed() const;

        /**
         * @brief dump the matrix to a stream (overrided)
         * @param fp the handle of a stream
        */
        virtual void dump(FILE* fp) const override;
        /**
         * @brief copy and create a new matrix from current matrix (as object)
         * @return handler of the new matrix
        */
        virtual handle<object> copy() const override;
    protected:
        /**
         * @brief the creation function
         * @note allocate the memory by the rows and cols
        */
        void _create();
        /**
         * @brief the release function
         * @note free the allocated memory
        */
        void _release();
    protected:
        /**
         * @brief the rows of a matrix
         * @note the rows of a matrix should be larger than 0
        */
        size_t _rows;
        /**
         * @brief the cols of a matrix
         * @note the cols of a matrix should be larger than 0
        */
        size_t _cols;
        /**
         * @brief the inner data pointer of matrix
        */
        double** _m = nullptr;
    };
    /**
     * @brief multiplication operator
     * @param n the multiplier number
     * @param m the multiplicand matrix
     * @return the result matrix
    */
    CGT_API matrix operator*(double n, const matrix& m);

    /**
     * @class matrix4x4
     * @brief the matrix that rows = 4 and cols = 4, derived from matrix
    */
    class CGT_API matrix4x4 : public matrix
    {
        cgt_claim(matrix4x4);
    public:
        /**
         * @brief constructor
         * @note will create an identity matrix
        */
        matrix4x4();
        /**
         * @brief constructor
         * @param r0 row0
         * @param r1 row1
         * @param r2 row2
         * @param r3 row3
        */
        matrix4x4(const vector4d& r0, const vector4d& r1,
            const vector4d& r2, const vector4d& r3);
        /**
         * @brief constructor
         * @param a00 set the element at (0,0)
         * @param a01 set the element at (0,1)
         * @param a02 set the element at (0,2)
         * @param a03 set the element at (0,3)
         * @param a10 set the element at (1,0)
         * @param a11 set the element at (1,1)
         * @param a12 set the element at (1,2)
         * @param a13 set the element at (1,3)
         * @param a20 set the element at (2,0)
         * @param a21 set the element at (2,1)
         * @param a22 set the element at (2,2)
         * @param a23 set the element at (2,3)
         * @param a30 set the element at (3,0)
         * @param a31 set the element at (3,1)
         * @param a32 set the element at (3,2)
         * @param a33 set the element at (3,3)
        */
        matrix4x4(double a00, double a01, double a02, double a03,
            double a10, double a11, double a12, double a13,
            double a20, double a21, double a22, double a23,
            double a30, double a31, double a32, double a33);
    };
}

#endif
