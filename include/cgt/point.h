/**
 * @file point.h
 * @copyright (c) Hotege
 * 
 * CGT points classes
*/

#if !defined(_CGT_POINT_H_)
#define _CGT_POINT_H_

#include "geometry.h"

namespace cgt
{
    /**
     * @class point
     * @brief the point in 3d coordinates, derived from class geometry
    */
    class CGT_API point : public geometry
    {
        cgt_claim(point);
    public:
        /**
         * @brief x component
        */
        double x;
        /**
         * @brief y component
        */
        double y;
        /**
         * @brief z component
        */
        double z;
    public:
        /**
         * @brief constructor
         * @note will create a point that equals (0,0,0)
        */
        point();
        /**
         * @brief constructor
         * @param _x appointed x component
         * @param _y appointed y component
         * @param _z appointed z component
        */
        point(double _x, double _y, double _z);
        /**
         * @brief constructor (from vector3d)
         * @param v the vector
        */
        point(const vector3d& v);

        /**
         * @brief assignment operator
         * @param pt the source point
         * @return point itself
         * @note this method will re-allocate the inside memory of point
        */
        point& operator=(const point& pt);
        /**
         * @brief comparison of two points
         * @param pt the point compared
         * @return boolean
         * @note this method will check each component in two points
        */
        bool operator==(const point& pt) const;
        /**
         * @brief opposition operator
         * @return the opposited point
         * @note will return point that components are reversed
        */
        point operator-() const;
        /**
         * @brief addition operator
         * @param pt the addend point
         * @return the result point
        */
        point operator+(const point& pt) const;
        /**
         * @brief subtraction operator
         * @param pt the subtrahend point
         * @return the result point
        */
        point operator-(const point& pt) const;
        /**
         * @brief multiplication operator
         * @param n the multiplier number
         * @return the result point
        */
        point operator*(double n) const;
        /**
         * @brief division operator
         * @param n the divisor number
         * @return the result point
        */
        point operator/(double n) const;
        /**
         * @brief self addition operator
         * @param pt the addend point
         * @return the result point (itself)
        */
        point& operator+=(const point& pt);
        /**
         * @brief self subtraction operator
         * @param pt the subtrahend point
         * @return the result point (itself)
        */
        point& operator-=(const point& pt);
        /**
         * @brief self multiplication operator
         * @param n the multiplier number
         * @return the result point (itself)
        */
        point& operator*=(double n);
        /**
         * @brief self division operator
         * @param n the divisor number
         * @return the result point (itself)
        */
        point& operator/=(double n);

        /**
         * @brief setting x, y and z
         * @param _x appointed x component
         * @param _y appointed y component
         * @param _z appointed z component
        */
        void set(double _x, double _y, double _z);

        /**
         * @brief dump the point to a stream (overrided)
         * @param fp the handle of a stream
         * @note the format is (x,y,z)
        */
        virtual void dump(FILE* fp) const override;
        /**
         * @brief copy and create a new point from current vector (as object)
         * @return handler of the new point
        */
        virtual handle<object> copy() const override;
        /**
         * @brief transformation of point
         * @param m the transformation matrix
        */
        virtual void transform(const matrix4x4& m) override;
    };
    using point3d = point;
}

#endif
