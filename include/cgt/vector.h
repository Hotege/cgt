/**
 * @file vector.h
 * @copyright (c) Hotege
 * 
 * CGT vector classes
 * vector is a term using more than a single number to express some quantities
 * in mathematics or physics
*/

#if !defined(_CGT_VECTOR_H_)
#define _CGT_VECTOR_H_

#include "object.h"

namespace cgt
{
    class point;

    /**
     * @class vector
     * @brief the basic class of vector, derived from class object
    */
    class CGT_API vector : public object
    {
        cgt_claim(vector);
    public:
        /**
         * @brief constructor of vector
         * @param d the dimension of a vector
         * @note the dimension should be larger than 0
        */
        vector(size_t d);
        /**
         * @brief copy constructor of vector
         * @param v the source vector
        */
        vector(const vector& v);
        /**
         * @brief destructor of vector
        */
        virtual ~vector();
        
        /**
         * @brief assignment operator
         * @param v the source vector
         * @return vector itself
         * @note this method will re-allocate the inside memory of vector
        */
        vector& operator=(const vector& v);
        /**
         * @brief comparison of two vectors
         * @param v the vector compared
         * @return boolean
         * @note this method will check each component in two vectors
        */
        bool operator==(const vector& v) const;
        /**
         * @brief opposition operator
         * @return the opposited vector
         * @note will return vector that components are reversed
         * @sa vector::reversed()
        */
        vector operator-() const;
        /**
         * @brief addition operator
         * @param v the addend vector
         * @return the result vector
         * @note the dimensions of two vectors should be equal
         * @note return itself if the dimensions are not equal
        */
        vector operator+(const vector& v) const;
        /**
         * @brief subtraction operator
         * @param v the subtrahend vector
         * @return the result vector
         * @note the dimensions of two vectors should be equal
         * @note return minuend if the dimensions are not equal
        */
        vector operator-(const vector& v) const;
        /**
         * @brief multiplication operator
         * @param n the multiplier number
         * @return the result vector
        */
        vector operator*(double n) const;
        /**
         * @brief division operator
         * @param n the divisor number
         * @return the result vector
        */
        vector operator/(double n) const;
        /**
         * @brief self addition operator
         * @param v the addend vector
         * @return the result vector (itself)
         * @note the dimensions of two vectors should be equal
         * @note return itself if the dimensions are not equal
        */
        vector& operator+=(const vector& v);
        /**
         * @brief self subtraction operator
         * @param v the subtrahend vector
         * @return the result vector (itself)
         * @note the dimensions of two vectors should be equal
         * @note return minuend if the dimensions are not equal
        */
        vector& operator-=(const vector& v);
        /**
         * @brief self multiplication operator
         * @param n the multiplier number
         * @return the result vector (itself)
        */
        vector& operator*=(double n);
        /**
         * @brief self division operator
         * @param n the divisor number
         * @return the result vector (itself)
        */
        vector& operator/=(double n);
        /**
         * @brief multiplication operator (dot of vectors)
         * @param v the multiplier vector
         * @return the result
         * @note dot = sum(vec[i] * v.vec[i]), i is from 0 to dimension - 1
         * @sa vector::dot()
        */
        double operator*(const vector& v) const;
        /**
         * @brief index operator
         * @param idx the index
         * @return the component
         * @note return vec[0] if idx is out of the range (0 to dimension - 1)
         * @sa vector::at(size_t idx)
        */
        double& operator[](size_t idx);
        /**
         * @brief index operator
         * @param idx the index
         * @return the constant component
         * @note return vec[0] if idx is out of the range (0 to dimension - 1)
         * @sa vector::at(size_t idx)
        */
        double operator[](size_t idx) const;

        /**
         * @brief index function
         * @param idx the index
         * @return the component
         * @note return vec[0] if idx is out of the range (0 to dimension - 1)
        */
        virtual double& at(size_t idx);
        /**
         * @brief index function
         * @param idx the index
         * @return the constant component
         * @note return vec[0] if idx is out of the range (0 to dimension - 1)
        */
        virtual double at(size_t idx) const;
        /**
         * @brief get the dimension of a vector
         * @return the dimension
        */
        size_t dimension() const;
        /**
         * @brief opposition function
         * @return the opposited vector
         * @note will reverse all signs of components in the vector
        */
        void reverse();
        /**
         * @brief opposition function
         * @return the opposited vector
         * @note will return vector that components are reversed
        */
        vector reversed() const;
        /**
         * @brief dot of vectors
         * @param v the multiplier vector
         * @return the result
         * @note dot = sum(vec[i] * v.vec[i]), i is from 0 to dimension - 1
        */
        double dot(const vector& v) const;
        /**
         * @brief get the squared magnitude of a vector
         * @return the squared magnitude
        */
        double square_magnitude() const;
        /**
         * @brief get the magnitude of a vector
         * @return the magnitude
         * @sa vector::squared_magnitude()
        */
        double magnitude() const;
        /**
         * @brief normalize a vector
         * @note all components will be divided by the magnitude
        */
        void normalize();
        /**
         * @brief get a normalized vector
         * @return normalized vector
        */
        vector normalized() const;

        /**
         * @brief dump the vector to a stream (overrided)
         * @param fp the handle of a stream
         * @note the format is (vec[0],vec[1],vec[2],...)
        */
        virtual void dump(FILE* fp) const override;
        /**
         * @brief copy and create a new vector from current vector (as object)
         * @return handler of the new vector
        */
        virtual handle<object> copy() const override;
    protected:
        /**
         * @brief the creation function
         * @note allocate the memory by the dimension
        */
        void _create();
        /**
         * @brief the release function
         * @note free the allocated memory
        */
        void _release();
    protected:
        /**
         * @brief the dimension of a vector
         * @note the dimension of a vector should be larger than 0
        */
        size_t _dimension;
        /**
         * @brief the inner data pointer of vector
        */
        double* _v = nullptr;
    };
    /**
     * @brief multiplication operator
     * @param n the multiplier number
     * @param v the multiplicand vector
     * @return the result vector
    */
    CGT_API vector operator*(double n, const vector& v);

    /**
     * @class vector2d
     * @brief the vector in 2d coordinates, derived from class vector, (x, y)
    */
    class CGT_API vector2d : public vector
    {
        cgt_claim(vector2d);
    public:
        /**
         * @brief the zero vector, (0,0)
        */
        static const vector2d zero;
        /**
         * @brief x axis, (1,0)
        */
        static const vector2d x_axis;
        /**
         * @brief y axis, (0,1)
        */
        static const vector2d y_axis;
    public:
        /**
         * @brief x component, vector2d use it instead of _v[0]
        */
        double x;
        /**
         * @brief y component, vector2d use it instead of _v[1]
        */
        double y;
    public:
        /**
         * @brief constructor
         * @note will create a vector that equals (0,0)
        */
        vector2d();
        /**
         * @brief constructor
         * @note create vector2d from basic vector
        */
        vector2d(const vector& v);
        /**
         * @brief constructor
         * @param _x appointed x component
         * @param _y appointed y component
        */
        explicit vector2d(double _x, double _y);

        /**
         * @brief index function
         * @param idx the index
         * @return the component
         * @note now returns x or y directly instead of _v[idx]
         * @note return x if idx is out of the range (0 to dimension - 1)
        */
        virtual double& at(size_t idx) override;
        /**
         * @brief index function
         * @param idx the index
         * @return the constant component
         * @note now returns x or y directly instead of _v[idx]
         * @note return x if idx is out of the range (0 to dimension - 1)
        */
        virtual double at(size_t idx) const override;
        /**
         * @brief setting x and y
         * @param _x appointed x component
         * @param _y appointed y component
        */
        void set(double _x, double _y);
    };

    /**
     * @class vector3d
     * @brief the vector in 3d coordinates, derived from class vector, (x,y,z)
    */
    class CGT_API vector3d : public vector
    {
        cgt_claim(vector3d);
    public:
        /**
         * @brief the zero vector, (0,0,0)
        */
        static const vector3d zero;
        /**
         * @brief x axis, (1,0,0)
        */
        static const vector3d x_axis;
        /**
         * @brief y axis, (0,1,0)
        */
        static const vector3d y_axis;
        /**
         * @brief z axis, (0,0,1)
        */
        static const vector3d z_axis;
    public:
        /**
         * @brief x component, vector3d use it instead of _v[0]
        */
        double x;
        /**
         * @brief y component, vector3d use it instead of _v[1]
        */
        double y;
        /**
         * @brief z component, vector3d use it instead of _v[2]
        */
        double z;
    public:
        /**
         * @brief constructor
         * @note will create a vector that equals (0,0,0)
        */
        vector3d();
        /**
         * @brief constructor
         * @note create vector3d from basic vector
        */
        vector3d(const vector& v);
        /**
         * @brief constructor (from point)
         * @param pt the point
        */
        vector3d(const point& pt);
        /**
         * @brief constructor (from two points)
         * @param s the start point
         * @param e the end point
        */
        vector3d(const point& s, const point& e);
        /**
         * @brief constructor
         * @param _x appointed x component
         * @param _y appointed y component
         * @param _z appointed z component
        */
        vector3d(double _x, double _y, double _z);

        /**
         * @brief xor operator (cross of vectors)
         * @param v the right v
         * @return the result vector
         * @sa vector3d::cross(const vector3d& v)
         * @note the result vector of cross is defined as following:
         *           |i  j  k |
         * v1 x v2 = |x1 y1 z1|=(y1z2-y2z1)i+(z1x2-z2x1)j+(x1y2-x2y1)k
         *           |x2 y2 z2|
        */
        vector3d operator^(const vector3d& v) const;

        /**
         * @brief index function
         * @param idx the index
         * @return the component
         * @note now returns x, y or z directly instead of _v[idx]
         * @note return x if idx is out of the range (0 to dimension - 1)
        */
        virtual double& at(size_t idx) override;
        /**
         * @brief index function
         * @param idx the index
         * @return the constant component
         * @note now returns x, y or z directly instead of _v[idx]
         * @note return x if idx is out of the range (0 to dimension - 1)
        */
        virtual double at(size_t idx) const override;
        /**
         * @brief setting x, y and z
         * @param _x appointed x component
         * @param _y appointed y component
         * @param _z appointed z component
        */
        void set(double _x, double _y, double _z);
        /**
         * @brief xor operator (cross of vectors)
         * @param v the right v
         * @return the result vector
         * @note the result vector of cross is defined as following:
         *           |i  j  k |
         * v1 x v2 = |x1 y1 z1|=(y1z2-y2z1)i+(z1x2-z2x1)j+(x1y2-x2y1)k
         *           |x2 y2 z2|
        */
        vector3d cross(const vector3d& v) const;
    };

    /**
     * @class vector4d
     * @brief the vector in 4d coordinates, derived from class vector, (x,y,z,w)
    */
    class CGT_API vector4d : public vector
    {
        cgt_claim(vector4d);
    public:
        /**
         * @brief the zero vector, (0,0,0)
        */
        static const vector4d zero;
        /**
         * @brief x axis, (1,0,0,0)
        */
        static const vector4d x_axis;
        /**
         * @brief y axis, (0,1,0,0)
        */
        static const vector4d y_axis;
        /**
         * @brief z axis, (0,0,1,0)
        */
        static const vector4d z_axis;
        /**
         * @brief w axis, (0,0,0,1)
        */
        static const vector4d w_axis;
    public:
        /**
         * @brief x component, vector4d use it instead of _v[0]
        */
        double x;
        /**
         * @brief y component, vector4d use it instead of _v[1]
        */
        double y;
        /**
         * @brief z component, vector4d use it instead of _v[2]
        */
        double z;
        /**
         * @brief w component, vector4d use it instead of _v[3]
        */
        double w;
    public:
        /**
         * @brief constructor
         * @note will create a vector that equals (0,0,0,0)
        */
        vector4d();
        /**
         * @brief constructor
         * @note create vector4d from basic vector
        */
        vector4d(const vector& v);
        /**
         * @brief constructor
         * @param _x appointed x component
         * @param _y appointed y component
         * @param _z appointed z component
         * @param _w appointed w component
        */
        vector4d(double _x, double _y, double _z, double _w);
        /**
         * @brief constructor
         * @param v the 3d vector
         * @param _w the 4th component
         * @note will create a vector that equals (v.x,v.y,v.z,_w)
        */
        vector4d(const vector3d& v, double _w = 0);

        /**
         * @brief index function
         * @param idx the index
         * @return the constant component
         * @note now returns x, y, z or w directly instead of _v[idx]
         * @note return x if idx is out of the range (0 to dimension - 1)
        */
        virtual double& at(size_t idx) override;
        /**
         * @brief index function
         * @param idx the index
         * @return the component
         * @note now returns x, y, z or w directly instead of _v[idx]
         * @note return x if idx is out of the range (0 to dimension - 1)
        */
        virtual double at(size_t idx) const override;
        /**
         * @brief setting x, y, z and w
         * @param _x appointed x component
         * @param _y appointed y component
         * @param _z appointed z component
         * @param _w appointed w component
        */
        void set(double _x, double _y, double _z, double _w);
    };
}

#endif
