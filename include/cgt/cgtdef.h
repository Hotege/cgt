/**
 * @file cgtdef.h
 * @copyright (c) Hotege
 * 
 * CGT definitions header file.
 * This file declares the necessary including header files and
 * defines the important macros and data types.
*/

#if !defined(_CGT_DEF_H_)
#define _CGT_DEF_H_

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <typeinfo>

/*
* Define UUID structure.
* The name of UUID varies with the Windows/UNIX platforms even if
* GUID in Windows and uuid_t in UNIX have same data structure.
*/

#if defined(_WIN32)
#include <objbase.h>

namespace cgt
{
    using uuid = GUID;
}
#else
#include <uuid/uuid.h>

namespace cgt
{
    using uuid = uuid_t;
}
#endif

/*
* Define export flags.
* It is important for Windows platform.
*/

#if defined(_WIN32) || defined(__MINGW32__) || defined(__MINGW64__)
#if defined(CGT_DLL)
#define CGT_API __declspec(dllexport)
#else
#define CGT_API __declspec(dllimport)
#endif
#else
#define CGT_API
#endif

#endif
